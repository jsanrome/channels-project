package com.example.projectchannels.view.channel.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.projectchannels.R

class ChannelsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_channels)
    }
}
