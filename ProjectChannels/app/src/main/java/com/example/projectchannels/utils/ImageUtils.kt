package com.example.projectchannels.utils

import android.widget.ImageView
import com.example.projectchannels.R
import com.squareup.picasso.Picasso

class ImageUtils {

    companion object {
        fun drawImage(url: String, view: ImageView) {
            Picasso.get().isLoggingEnabled
            Picasso.get().load(url).error(R.drawable.empty).into(view)
        }
    }

}