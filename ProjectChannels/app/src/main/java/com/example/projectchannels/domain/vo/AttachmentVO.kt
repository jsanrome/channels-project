package com.example.projectchannels.domain.vo

import java.io.Serializable

data class AttachmentVO(
    val responseElementType: String?=null,
    val assetId: String?=null,
    val name: String?=null,
    val assetName: String?=null,
    val value: String?=null
):Serializable