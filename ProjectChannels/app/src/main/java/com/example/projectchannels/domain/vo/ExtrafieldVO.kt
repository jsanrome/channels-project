package com.example.projectchannels.domain.vo

import java.io.Serializable

data class ExtrafieldVO(
    val name: String?= null,
    val responseElementType: String?= null,
    val value: String?= null
): Serializable