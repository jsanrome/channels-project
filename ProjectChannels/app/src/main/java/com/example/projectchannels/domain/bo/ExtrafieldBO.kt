package com.example.projectchannels.domain.bo

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ExtrafieldBO(
    val name: String?= null,
    val responseElementType: String?= null,
    val value: String?= null
): Serializable