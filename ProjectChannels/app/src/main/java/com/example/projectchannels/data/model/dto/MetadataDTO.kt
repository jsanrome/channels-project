package com.example.projectchannels.data.model.dto

import com.example.projectchannels.data.base.BaseDTO
import com.google.gson.annotations.SerializedName

data class MetadataDTO(
    @SerializedName("fullLength")
    val fullLength: Int?=null,
    @SerializedName("request")
    val request: String?=null,
    @SerializedName("timestamp")
    val timestamp: Long?=null
):BaseDTO()