package com.example.projectchannels.domain.bo

import com.example.projectchannels.data.base.BaseDTO
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class MetadataBO(
    val fullLength: Int?=null,
    val request: String?=null,
    val timestamp: Long?=null
): Serializable