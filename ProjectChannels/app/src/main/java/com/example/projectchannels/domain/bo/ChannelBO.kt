package com.example.projectchannels.domain.bo

import com.example.projectchannels.domain.vo.MetadataVO
import com.example.projectchannels.domain.vo.ResponseVO
import java.io.Serializable

data class ChannelBO(
    val metadata: MetadataBO?=null,
    val response: List<ResponseBO>?=null
): Serializable