package com.example.projectchannels.domain.vo

import java.io.Serializable

data class ChannelVO(
    val metadata: MetadataVO?=null,
    val response: List<ResponseVO>?=null
): Serializable