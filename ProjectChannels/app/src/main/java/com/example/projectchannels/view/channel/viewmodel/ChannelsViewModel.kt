package com.example.projectchannels.view.channel.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.projectchannels.data.repository.apidatasource.ChannelService
import com.example.projectchannels.domain.bo.ChannelBO
import com.example.projectchannels.domain.mappers.MapperChannelBOToVO
import com.example.projectchannels.domain.usescase.ChannelUseCase
import com.example.projectchannels.domain.vo.ChannelVO

class ChannelsViewModel : ViewModel() {

    val channelUseCase = ChannelUseCase()
    val ldChannels = MutableLiveData<ChannelVO>()
    val ldError = MutableLiveData<Boolean>()

    fun onSendJSONContent(jsonContent : String){
        channelUseCase.onGetChannelList(jsonContent, {
            //Setear error para verlo en la vista
            ldError.value = true
        }) {
            // respuesta ok
            ldChannels.value =  MapperChannelBOToVO.convertChannelBOToVO(it)
        }
    }}
