package com.example.projectchannels.data.model.dto

import com.google.gson.annotations.SerializedName

data class ResponseDTO(
    @SerializedName("affiliation")
    val affiliation: String?=null,
    @SerializedName("attachments")
    val attachments: List<AttachmentDTO>?=null,
    @SerializedName("bitrate")
    val bitrate: String?=null,
    @SerializedName("category")
    val category: String?=null,
    @SerializedName("contentDefinition")
    val contentDefinition: String?=null,
    @SerializedName("description")
    val description: String?=null,
    @SerializedName("encoding")
    val encoding: String?=null,
    @SerializedName("externalChannelId")
    val externalChannelId: String?=null,
    @SerializedName("extrafields")
    val extrafields: List<ExtrafieldDTO>?=null,
    @SerializedName("flags")
    val flags: Int?=null,
    @SerializedName("id")
    val id: Float?=null,
    @SerializedName("identifier")
    val identifier: String?=null,
    @SerializedName("interactiveUrl")
    val interactiveUrl: String?=null,
    @SerializedName("ip")
    val ip: String?=null,
    @SerializedName("longName")
    val longName: String?=null,
    @SerializedName("name")
    val name: String?=null,
    @SerializedName("number")
    val number: Int?=null,
    @SerializedName("originalNetworkId")
    val originalNetworkId: String?=null,
    @SerializedName("port")
    val port: Int?=null,
    @SerializedName("prLevel")
    val prLevel: Int?=null,
    @SerializedName("prName")
    val prName: String?=null,
    @SerializedName("responseElementType")
    val responseElementType: String?=null,
    @SerializedName("serviceId")
    val serviceId: String?=null,
    @SerializedName("simultaneousViewsLimit")
    val simultaneousViewsLimit: String?=null,
    @SerializedName("sourceType")
    val sourceType: String?=null,
    @SerializedName("transportStreamId")
    val transportStreamId: String?=null,
    @SerializedName("url")
    val url: String?=null
)