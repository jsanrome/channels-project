package com.example.projectchannels.domain.vo

import java.io.Serializable

data class ResponseVO(
    val affiliation: String?=null,
    val attachments: List<AttachmentVO>?=null,
    val bitrate: String?=null,
    val category: String?=null,
    val contentDefinition: String?=null,
    val description: String?=null,
    val encoding: String?=null,
    val externalChannelId: String?=null,
    val extrafields: List<ExtrafieldVO>?=null,
    val flags: Int?=null,
    val id: Float?=null,
    val identifier: String?=null,
    val interactiveUrl: String?=null,
    val ip: String?=null,
    val longName: String?=null,
    val name: String?=null,
    val number: Int?=null,
    val originalNetworkId: String?=null,
    val port: Int?=null,
    val prLevel: Int?=null,
    val prName: String?=null,
    val responseElementType: String?=null,
    val serviceId: String?=null,
    val simultaneousViewsLimit: String?=null,
    val sourceType: String?=null,
    val transportStreamId: String?=null,
    val url: String?=null
): Serializable