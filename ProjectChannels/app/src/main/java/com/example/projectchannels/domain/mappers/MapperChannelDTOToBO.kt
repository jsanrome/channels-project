package com.example.projectchannels.domain.mappers

import com.example.projectchannels.data.model.dto.*
import com.example.projectchannels.domain.bo.*

class MapperChannelDTOToBO {
    companion object {

        fun convertChannelDTOToBO(model: ChannelDTO?) =
            ChannelBO(
                metadata = convertMetadataToBO(model?.metadata),
                response = convertListResponseTOBO(model?.response)
            )

        fun convertMetadataToBO(model: MetadataDTO?) =
            MetadataBO(model?.fullLength, model?.request, model?.timestamp)

        fun convertResponseToBO(model: ResponseDTO?) =
            ResponseBO(name = model?.name, attachments = convertListAttachmentToBO(model?.attachments), extrafields = convertListExtrafieldToBO(model?.extrafields))

        fun convertListResponseTOBO(model: List<ResponseDTO>?): List<ResponseBO> {

            val listResponseBO = arrayListOf<ResponseBO>()
            model?.forEach {
                listResponseBO.add(convertResponseToBO(it))
            }
            return listResponseBO
        }

        fun convertAttachmentToBO(model: AttachmentDTO?) = AttachmentBO(
            model?.responseElementType,
            model?.assetId,
            model?.name,
            model?.assetName,
            model?.value
        )
        fun convertAttachmentToBO(model: ExtrafieldDTO?) = ExtrafieldBO(
            model?.responseElementType,
            model?.name,
            model?.value
        )

        fun convertListAttachmentToBO(model: List<AttachmentDTO>?): List<AttachmentBO> {
            val attachmentBO = arrayListOf<AttachmentBO>()
            model?.forEach {
                attachmentBO.add(convertAttachmentToBO(it))
            }
            return attachmentBO
        }
           fun convertListExtrafieldToBO(model: List<ExtrafieldDTO>?): List<ExtrafieldBO> {
            val extrafieldListBO = arrayListOf<ExtrafieldBO>()
            model?.forEach {
                extrafieldListBO.add(convertAttachmentToBO(it))
            }
            return extrafieldListBO
        }

    }
}