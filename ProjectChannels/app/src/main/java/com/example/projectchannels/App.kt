package com.example.projectchannels

import android.app.Application

class App : Application() {
    val instance by lazy { this }
}