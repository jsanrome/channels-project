package com.example.projectchannels.domain.bo

import com.example.projectchannels.data.base.BaseDTO
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class AttachmentBO(
    val responseElementType: String?=null,
    val assetId: String?=null,
    val name: String?=null,
    val assetName: String?=null,
    val value: String?=null
):Serializable