package com.example.projectchannels.domain.usescase

import com.example.projectchannels.data.repository.apidatasource.ChannelService
import com.example.projectchannels.domain.bo.ChannelBO
import com.example.projectchannels.domain.mappers.MapperChannelDTOToBO.Companion.convertChannelDTOToBO

class ChannelUseCase{

    private val channelService = ChannelService()

    fun onGetChannelList(jsonContent : String, failure: (String) -> Unit, success: (ChannelBO) -> Unit) {
        channelService.getChannelsFromJson(jsonContent,{
            // Error lambda
        },{
            val channelDTO = convertChannelDTOToBO(it)
            success(channelDTO)
            // Success lmbda -> DTO a BO  y BO a VO
        })

    }
}