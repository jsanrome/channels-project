package com.example.projectchannels.data.model.dto

import com.example.projectchannels.data.base.BaseDTO
import com.google.gson.annotations.SerializedName

data class AttachmentDTO(
    @SerializedName("responseElementType")
    val responseElementType: String?=null,
    @SerializedName("assetId")
    val assetId: String?=null,
    @SerializedName("name")
    val name: String?=null,
    @SerializedName("assetName")
    val assetName: String?=null,
    @SerializedName("value")
    val value: String?=null
):BaseDTO()